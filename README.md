# Frontend assignment exercise

DA CONCEICAO Dylan - 2021

## Assignment

### Goal

The goal of this assignment is to convert the provided designs below to a working frontend using HTML, CSS and JS.

- [Homepage - Desktop](designs/Red%20Alert%20-%20Homepage%20-%20Desktop.png)

- [Homepage - Mobile](designs/Red%20Alert%20-%20Homepage%20-%20Mobile.png)

- [More Information Popup - Desktop](designs/Red%20Alert%20-%20More%20Information%20Popup%20-%20Desktop.png)

- [Q&A Expanded - Mobile](designs/Red%20Alert%20-%20QA%20Expanded%20-%20Mobile.png)

### Development notes

#### Access

Deployed on my Raspberry Pi NGINX server at this address : [Red Alert (http://dylan-dco.ddns.net)](http://dylan-dco.ddns.net/)

Unfortunately in HTTP only, I have not configured any SSL certificate.

#### Styles

- [BEM methodology](http://getbem.com/)

- Components / Page / Partials / Utilities folder structure

- Added a fonts import using Webpack file-loader

- Variables file

- Breakpoints mixin

#### Components

##### Collapsable

###### HTML

```html
<ul id="my-list" data-collapsable>
  <li>
    <button data-trigger>Title</button>

    <div data-content>
      <p>Lorem ipsum</p>
    </div>
  </li>
</ul>
```

###### JS

```javascript
let myCollapsable = new Collapsable(document.getElementById("my-list"), options);
```

###### OPTIONS

| Option             | Type       | Default  | Description                                     |
| ------------------ | ---------- | -------- | ----------------------------------------------- |
| `openedClass`      | `string`   | `opened` | Active class                                    |
| `collapseDuration` | `string`   | `350`    | Content appearance transition time in `ms`      |
| `exclusive`        | `boolean`  | `false`  | Only one opened item                            |
| `onClose`          | `function` | `null`   | Callback triggered when an item has been closed |
| `onOpen`           | `function` | `null`   | Callback triggered when an item has been opened |

##### Modal

###### HTML

```html
<!-- Modal button trigger -->

<button id="my-modal-button">Open modal</button>

<!-- Modal content -->

<div id="my-modal-content">Hello world</div>
```

###### JS

```javascript
let myModal = new Modal(document.getElementById("my-modal-content"), options);

myModal.addTrigger(document.getElementById("my-modal-button"));
```

###### OPTIONS

| Option       | Type       | Default | Description                                                                  |
| ------------ | ---------- | ------- | ---------------------------------------------------------------------------- |
| `title`      | `string`   |         | Modal title                                                                  |
| `id`         | `string`   |         | Modal DOM id                                                                 |
| `classes`    | `string`   |         | Modal DOM classes                                                            |
| `closeLabel` | `string`   | `Close` | Modal close button label                                                     |
| `onClose`    | `function` | `null`  | Callback triggered when the modal has been closed                            |
| `onOpen`     | `function` | `null`  | Callback triggered when the modal has been opened                            |
| `onRendered` | `function` | `null`  | Callback triggered when the modal has been created and present in DOM (once) |

###### METHODS

| Option          | Parameter type | Description                                                                  |
| --------------- | -------------- | ---------------------------------------------------------------------------- |
| `addTrigger`    | `element`      | Attach a click event to open the modal on an element (one modal per element) |
| `removeTrigger` | `element`      | Detach the click event of an element                                         |

### Scope

- [x] Responsive design

- Mobile-first approach with media-breakpoint-up mixins

- Tested on mobile, tablets and desktop using Chrome devtools and [Viewport Resizer Chrome extension](https://chrome.google.com/webstore/detail/viewport-resizer-%E2%80%93-respon/kapnjjcfcncngkadhpmijlkblpibdcgm)

- [x] Accessible

- Validated AAA conformance with [SiteImprove Chrome extension](https://chrome.google.com/webstore/detail/siteimprove-accessibility/efcfolpjihicnikpmhnmphjhhpiclljc) (https://siteimprove.com/)

- [x] Being able to open and close the FAQ items

- [x] Only one FAQ item is allowed to be open

- Created a "Collapsable" generic component using JavaScript class with options

- [x] Being able to open and close the modal box

- Created a "Modal" generic component using JavaScript class with options

- [x] Compatible with latest Chrome and Firefox versions

- Tested in latest Chrome and Firefox, plus using [BrowserStack](https://www.browserstack.com/)

## Design specifics

- Colors used:

- Dark grey: `#2B2D42`

- Grey: `#8D99AE`

- Light grey: `#EDF2F4`

- Dark red: `#7F1028`

- Light red: `#AD646D`

- White: `#FFFFFF`

- Font used is `Helvetica`

- Static assets

- [Product image 1 png](./public/assets/images/product-1-transparent.png)

- [Product image 1 webp](./public/assets/images/product-1-transparent.webp)

- [Product image 2 png](./public/assets/images/product-2-transparent.png)

- [Product image 2 webp](./public/assets/images/product-2-transparent.webp)

## Usage

### Global dependencies

you need to have a recent node.js installation and yarn:

- [Node.js](https://nodejs.org/)

- [Yarn](https://yarnpkg.com/)

### Install dependencies

```





yarn install





```

### Run development server

```





yarn dev





```

Will open your default browser to http://localhost:8080/public

Webpack will watch for changes in the `./src` directory and output the bundled assets to `./public/assets`. In parellel, the development server will watch for changes in the `./public` directory and live reload the browser.

### Build production bundles

```





yarn build





```

Will compile, minify and autoprefix Sass to CSS. Will Minify and uglify JavaScript and output the bundled assets to `./public/assets`.

### Assets

All the `designs, icons and images` needed to implement the designs are added to the `./public/assets/images` directory.

## Stack

The boilerplate consists of following tech stack and should run out of the box with a basic setup -

- Webpack

- Sass compilation (and minification/autoprefixing in production)

- ES6+ transpilation (and minification/uglyfication in production)

- ES Modules

## Questions

If you have any questions while working on the exercise feel free to reach out. We will be happy to help.

Happy coding!
