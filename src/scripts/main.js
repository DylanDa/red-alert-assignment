import Collapsable from "./collapsable";
import Modal from "./modal";

document.addEventListener("DOMContentLoaded", () => {
  console.log("js is processed");

  //Collapse lists DOM init
  let lists = document.querySelectorAll("[data-collapsable]");
  let collapsableInstances = [];
  for (let i = 0; i < lists.length; i++) {
    collapsableInstances.push(
      new Collapsable(lists[i], {
        exclusive: true,
        collapseDuration: 250,
        onOpen: (e) => {
          console.log("One collapsable has been opened.");
        },
        onClose: (e) => {
          console.log("One collapsable has been closed.");
        },
      })
    );
  }

  //Modal DOM init
  let modalContent = document.getElementById("modal-demo-content");
  let modal = new Modal(modalContent, {
    title: "More information",
    id: "products-info",
    classes: "products-info",
    onOpen: (e) => {
      console.log("A modal has been opened.");
    },
    onClose: (e) => {
      console.log("A modal has been closed.");
    },
    onRendered: (e) => {
      console.log("A modal has been rendered.");
    },
  });
  modal.addTrigger(document.getElementById("more_info"));
});
