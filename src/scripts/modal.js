import PerfectScrollbar from "perfect-scrollbar";
export default class Modal {
  /**
   *
   * @param content Modal content
   * @param options Options override
   */
  constructor(content, options) {
    // Modal config (with user merge)
    this.conf = {
      title: "",
      id: "",
      classes: "",
      closeLabel: "Close",
      onClose: null,
      onOpen: null,
      onRendered: null,
      ...options,
    };

    // User callbacks ('this' binding)
    if (this.conf.onClose) this.conf.onClose = this.conf.onClose.bind(this);
    if (this.conf.onOpen) this.conf.onOpen = this.conf.onOpen.bind(this);
    if (this.conf.onRendered) this.conf.onRendered = this.conf.onRendered.bind(this);

    // Attributes
    this.triggers = [];
    this.modalUserContent = content;

    // Create modal in DOM once
    this._createModal();
  }

  // Add a trigger to modal
  addTrigger(trigger) {
    if (trigger) {
      //A trigger can only be associated with one modal at a time
      if (trigger.modalTrigger) trigger.removeEventListener("click", trigger.modalTrigger); //Removing old event listener
      trigger.modalTrigger = this._openModal.bind(this); //Set new trigger

      trigger.addEventListener("click", trigger.modalTrigger);
      this.triggers.push(trigger);
    }
  }

  // Remove a trigger from modal
  removeTrigger(trigger) {
    if (trigger) {
      trigger.removeEventListener("click", trigger.modalTrigger);
      this.triggers = this.triggers.filter((item) => item !== trigger);
    }
  }

  // Open modal
  _openModal(event) {
    // Lock body
    document.body.style.overflowY = "hidden";

    // Show modal
    this.modalContent.classList.add("modal--opened");
    this.modalContainer.setAttribute("aria-hidden", "false");

    // Init scrollbar
    if (this.modalBodyScrollbar) this.modalBodyScrollbar.update();
    else
      this.modalBodyScrollbar = new PerfectScrollbar(this.modalBody, {
        wheelSpeed: 0.5,
        wheelPropagation: true,
        minScrollbarLength: 20,
      });

    // User callback
    if (this.conf.onOpen) {
      this.conf.onOpen();
    }
  }

  // Close modal
  _closeModal(event) {
    // Animation out class
    this.modalContent.classList.add("modal--closing");

    setTimeout(() => {
      // Remove animation out class
      this.modalContent.classList.remove("modal--closing");

      // Unlock body
      document.body.style.overflowY = "";

      // Hide modal
      this.modalContent.classList.remove("modal--opened");
      this.modalContainer.setAttribute("aria-hidden", "true");

      // User callback
      if (this.conf.onClose) {
        this.conf.onClose();
      }
    }, 250);
  }

  // Create modal in the DOM
  _createModal() {
    this.modalContainer = document.createElement("div");
    this.modalContainer.classList.add("modal-container");
    this.modalContainer.setAttribute("aria-hidden", "true");
    this.modalContainer.innerHTML = this._getTemplate();
    this.modalContent = this.modalContainer.querySelector(".modal");
    this.modalBody = this.modalContainer.querySelector(".modal__body");
    this.modalContainer.querySelector("[data-dismiss]").addEventListener("click", this._closeModal.bind(this));
    document.body.appendChild(this.modalContainer);

    // User callback
    if (this.conf.onRendered) {
      this.conf.onRendered();
    }
  }

  _removeModal() {
    document.body.removeChild(this.modalContainer);
  }

  // Modal template
  _getTemplate() {
    return `
			<div id="${this.conf.id}" class="modal ${this.conf.classes}">
				<div class="modal__content ${this.conf.id ? this.conf.id + "-content" : ""}">
					<div class="modal__header ${this.conf.id ? this.conf.id + "-header" : ""}">
                        <h1>${this.conf.title}</h1>
                    </div>
                    <div class="modal__body ${this.conf.id ? this.conf.id + "-body" : ""}">
                        ${this.modalUserContent ? this.modalUserContent.innerHTML : ""}
                    </div>
                    <div class="modal__footer ${this.conf.id ? this.conf.id + "-footer" : ""}">
                        <button type="button" class="btn btn--primary" data-dismiss="modal" title="Click here to close" aria-label="Close modal">${this.conf.closeLabel}</button>
                    </div>
				</div>
			</div>
		`;
  }
}
