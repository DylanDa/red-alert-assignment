export default class Collapsable {
  /**
   *
   * @param elem Collapsable list
   * @param options Options override
   */
  constructor(elem, options) {
    // Collapsable config (with user merge)
    this.conf = {
      openedClass: "opened",
      collapseDuration: "350",
      exclusive: true,
      onClose: null,
      onOpen: null,
      ...options,
    };

    // User callbacks ('this' binding)
    if (this.conf.onClose) this.conf.onClose = this.conf.onClose.bind(this);
    if (this.conf.onOpen) this.conf.onOpen = this.conf.onOpen.bind(this);

    // Attributes
    this.list = elem;
    this.items = elem.children;
    this.triggers = elem.querySelectorAll("[data-trigger]");
    this.contents = elem.querySelectorAll("[data-content]");

    // Set click triggers
    this._bindEvents();

    // Set custom transition duration
    this._setTransition();
  }

  // Bind triggers + resize event
  _bindEvents() {
    for (let i = 0; i < this.triggers.length; i++) {
      this.triggers[i].addEventListener("click", this._collapse.bind(this));
    }

    window.addEventListener("resize", this._adjustScrollheight.bind(this));
  }

  // Set transition
  _setTransition() {
    for (let i = 0; i < this.contents.length; i++) {
      this.contents[i].style.transitionDuration = this.conf.collapseDuration + "ms";
    }
  }

  // Toggle collapse
  _collapse(event) {
    let curTrigger = event.target;
    let curItem = curTrigger.parentElement;
    let curItemContent = curItem.querySelector("[data-content]");

    if (!curItem.classList.contains(this.conf.openedClass)) {
      if (this.conf.exclusive) {
        // Close all items
        for (let i = 0; i < this.items.length; i++) {
          let item = this.items[i];
          item.classList.remove(this.conf.openedClass);
          let itemContent = item.querySelector("[data-content]");
          itemContent.style.maxHeight = "0px";
        }
      }
      // Open current item
      curItem.classList.add(this.conf.openedClass);
      curItemContent.style.maxHeight = curItemContent.scrollHeight + "px";

      // User callback
      if (this.conf.onOpen) {
        this.conf.onOpen();
      }
    } else {
      // Close current item
      curItem.classList.remove(this.conf.openedClass);
      curItemContent.style.maxHeight = "0px";

      // User callback
      if (this.conf.onClose) {
        this.conf.onClose();
      }
    }
  }

  // Adjust visible content scrollHeight on resize
  _adjustScrollheight() {
    if (this.list) {
      let visibleContents = this.list.querySelectorAll(`.${this.conf.openedClass} [data-content]`);
      for (let i = 0; i < visibleContents.length; i++) {
        let content = visibleContents[i];
        content.style.maxHeight = content.scrollHeight + "px";
      }
    }
  }
}
